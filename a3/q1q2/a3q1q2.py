#!/usr/local/bin/python3

import sys
import os
import logging

import numpy as np
from skimage import io, data, filters, restoration, img_as_float
from skimage import __version__ as skimage_version


def gaussian_kernel(dim, sigma):
    "Generate a Gaussian dim x dim kernel with standard deviation sigma"

    # create a 2D impulse kernel (1 surrounded by zeros)
    impulse = np.zeros((dim, dim))
    impulse[dim//2, dim//2] = 1.0

    # Gaussian smooth the impulse to get a Gaussian
    kernel = filters.gaussian(impulse, sigma)
    kernel_sum = kernel.sum()
    logging.info("Kernel magnitude: {}".format(kernel_sum))
    if kernel_sum < 0.999:
        fmt = "{0}x{0} kernel with sigma {1} results in truncation, total kernel magnitude only {2}"
        logging.warning(fmt.format(dim, sigma, kernel_sum))

    # Normalize and return
    return kernel / kernel_sum


def horizontal_kernel(dim):
    "Generate a horizontal dim x dim kernel with an even straight horizontal line"
    kernel = np.zeros((dim, dim))
    kernel[dim // 2, :] = np.ones(dim) / dim
    return kernel


def processImage(img, include_original=False, gauss=(21, 3), horizontal=()):
    """Yield (key, image) pairs for images based on provided image
       Include original, with a key of None, if include_original flag is set
    """
    if include_original:
        yield None, img

    # Create a 21x21 gaussian blur kernel with sigma = 3
    print("Generating blur kernels...")
    g3kernel = gaussian_kernel(*gauss)
    hkernels = [(hdim, horizontal_kernel(hdim)) for hdim in horizontal]

    for i in (10, 20, 50, 250):
        print("Performing {} iterations of Richardson-Lucy deconvolution...".format(i))
        yield "Gauss3-RL{}".format(i), restoration.richardson_lucy(img, g3kernel, iterations=i)
        for hdim, hkernel in hkernels:
            yield "H{}-RL{}".format(hdim, i), restoration.richardson_lucy(img, hkernel, iterations=i)


DEFAULT_FILENAMES = ["a3_im1.jpg"]

def main(args):
    print("NumPy v" + np.__version__)
#     print("OpenCV v" + cv2.__version__)
    print("scikit-image v" + skimage_version)

    image_filenames = args[1:] or DEFAULT_FILENAMES
    for fn in image_filenames:
        assert os.path.isfile(fn)

    images = dict((fn, img_as_float(io.imread(fn))) for fn in image_filenames)

    # Adding the horizontal motion-blurred clock image from skimage.data
    images["clock.jpg"] = img_as_float(data.clock())

    for fn, img in images.items():
        basename, extension = fn.rsplit('.', 1)
        hdims = (21, 37, 45) if basename == "clock" else ()
        for key, processed_img in processImage(img, include_original=True, horizontal=hdims):
            out_fn = "{}-{}.{}".format(basename, key or "original", extension)
            print("Writing {!r}".format(out_fn))
            io.imsave(out_fn, processed_img)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
