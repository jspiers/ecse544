#!/usr/local/bin/python3

import sys
import os
import logging

import numpy as np
from pyzbar import pyzbar
from skimage import io, data, filters, restoration, img_as_float, color
from skimage import __version__ as skimage_version


def gaussian_kernel(dim, sigma):
    "Generate a Gaussian dim x dim kernel with standard deviation sigma"
    dim = int(round(dim))

    # create a 2D impulse kernel (1 surrounded by zeros)
    impulse = np.zeros((dim, dim))
    impulse[dim//2, dim//2] = 1.0

    # Gaussian smooth the impulse to get a Gaussian
    kernel = filters.gaussian(impulse, sigma)
    kernel_sum = kernel.sum()
    logging.info("Kernel magnitude: {}".format(kernel_sum))
    if kernel_sum < 0.999:
        fmt = "{0}x{0} kernel with sigma {1} results in truncation, total kernel magnitude only {2}"
        logging.warning(fmt.format(dim, sigma, kernel_sum))

    # Normalize and return
    return kernel / kernel_sum


def processImage(img, include_original=False, sigmas=(2,4,5,6,8)):
    """Yield (key, image) pairs for images based on provided image
       Include original, with a key of None, if include_original flag is set
    """
    if include_original:
        yield None, img

    if len(img.shape) > 2:
        assert img.shape[2] == 3, img.shape
        img = color.rgb2gray(img)

#     print("sigma estimate: {}".format(restoration.estimate_sigma(img)))

    print("Generating blur kernels...")
    kernels = dict(("Gauss{0}_{1}x{1}".format(sigma, mult*sigma), gaussian_kernel(m * sigma, sigma))
                   for sigma in sigmas for m in (3, 5, 7))

    for kname, kernel in kernels.items():
        print("kernel =", kname)
        for i in (10, 15, 20, 35, 50, 70, 100):
            print("Performing {} iterations of Richardson-Lucy deconvolution...".format(i))
            yield "{}-RL{}".format(kname, i), restoration.richardson_lucy(img, kernel, iterations=i)
#         for balance in (0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 10.0, 100.0, 200.0, 500.0):
#             print("Performing Wiener-Hunt deconvolution with balance={}...".format(balance))
#             yield "{}-Wiener{}".format(kname, balance), restoration.wiener(img, kernel, balance)


DEFAULT_FILENAMES = []

def main(args):
    print("NumPy v" + np.__version__)
#     print("OpenCV v" + cv2.__version__)
    print("scikit-image v" + skimage_version)

    image_filenames = args[1:] or DEFAULT_FILENAMES
    for fn in image_filenames:
        assert os.path.isfile(fn)

    images = dict((fn, img_as_float(io.imread(fn))) for fn in image_filenames)

    for fn, img in images.items():
        basename, extension = fn.rsplit('.', 1)
        for key, processed_img in processImage(img, include_original=True):
            out_fn = "{}-{}.{}".format(basename, key or "original", extension)
            print("Writing {!r}".format(out_fn))
            io.imsave(out_fn, processed_img)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
