img = imread('blur.jpg')

for sigma = 2:8
    dim = sigma * 6 + 1 % guaranteed to be odd
    psf = fspecial('gaussian', dim, sigma)
    gauss = sprintf('gauss%d-%dx%d', sigma, dim, dim)
    
    % blind deconvolution
    deblurred = deconvblind(img, psf)
    
    filename = sprintf('matlab-deconvblind-%s.jpg', gauss)
    imwrite(deblurred, filename)
    for i = 0:3
        iter = 10 + 20 * i
        deblurred = deconvlucy(img, psf, iter)
        filename = sprintf('matlab-deconvlucy-%s-rl%d.jpg', gauss, iter)
        imwrite(deblurred, filename)
    end
    deblurred = deconvreg(img, psf)
    filename = sprintf('matlab-deconvreg-%s.jpg', gauss)
    imwrite(deblurred, filename)
end