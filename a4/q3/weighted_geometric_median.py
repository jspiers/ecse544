#!/usr/bin/env python3

# Based on "geometric_median" code shared by Stack Overflow user "orlp" at
# "https://stackoverflow.com/questions/30299267/geometric-median-of-multidimensional-points"
# which is ostensibly an implementation of
# Vardi and Zhang "The multivariate L1-median and associated data depth", 1999.
#
# Licensed under "Stack Overflow license terms (?)" and "zlib license"
#

import numpy as np
from scipy.spatial.distance import cdist, euclidean

def weightedGeometricMedian(X, eps=1e-5, weights=None):
    """ Computes the geometric median of the vectors in X, with weights applied to each vector's
        contribution
        In other words, effectively multiply the number of samples of each vector by its
        corresponding weight before computing the geometric median of the samples
    """
    if weights is None:
        weights = np.ones(X.shape[0]) / X.shape[0]  # default to equal weighting per vector
        # print("default weights", weights)

    weights = weights.reshape(weights.size, 1)
    assert weights.shape == (X.shape[0], 1)
    assert abs(weights.sum() - 1.0) < 1e-9
    # print("weights.shape", weights.shape)

    y = np.mean(X, 0)

    # i = 0
    while True:
        # i += 1
        # print("iteration {}:\n{}".format(i, y))

        D = cdist(X, [y])
        nonzeros = (D != 0)[:, 0]

        # Using weights instead of '1' as numerator to weight the contribution of each vector
        #
        Dinv = weights / D[nonzeros]
        Dinvs = np.sum(Dinv)
        W = Dinv / Dinvs
        T = np.sum(W * X[nonzeros], 0)

        num_zeros = len(X) - np.sum(nonzeros)
        if num_zeros == 0:
            y1 = T
        elif num_zeros == len(X):
            return y
        else:
            R = (T - y) * Dinvs
            r = np.linalg.norm(R)
            rinv = 0 if r == 0 else num_zeros/r
            y1 = max(0, 1-rinv)*T + min(1, rinv)*y

        if euclidean(y, y1) < eps:
            return y1

        y = y1


if __name__ == "__main__":
    a = np.ones((3, 3))
    a[1, :] = [0.5, 0.5, 0.5]
    # print('a\n', a)

    b = np.zeros((3, 3))
    # print('b\n', b)

    points = np.array([a.reshape(1, 9)[0], b.reshape(1, 9)[0], b.reshape(1, 9)[0]])
    print('points\n', points)

    for custom_weights in (None, np.array([0.8, 0.1, 0.1]), np.array([0.5, 0.25, 0.25])):
        print("custom_weights:", custom_weights)
        # print("custom_weights.shape", custom_weights.shape)

        gmedian = weightedGeometricMedian(points, weights=custom_weights)
        print("weighted geometric median\n", gmedian)
