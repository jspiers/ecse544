#!/usr/bin/env python3

import sys
import os
import math
import functools

import numpy as np
from skimage import img_as_float, img_as_ubyte
from skimage import __version__ as skimage_version
import cv2 as cv
from scipy.spatial.distance import cdist, euclidean, sqeuclidean

from weighted_geometric_median import weightedGeometricMedian


# def memoize(func):
#     cache = func.cache = {}
#
#     @functools.wraps(func)
#     def memoized_func(*args, **kwargs):
#         key = str(args) + str(kwargs)
#         if key not in cache:
#             cache[key] = func(*args, **kwargs)
#         return cache[key]
#     return memoized_func
#
# @memoize
# def fibonacci(n):
#     if n == 0:return 0
#     if n == 1:return 1
#     else: return fib(n-1) + fib(n-2)
#

def patch(img, yx, p):
    y, x = yx
    size = (2*p + 1) * (2*p + 1)
    return img[y-p:y+p+1, x-p:x+p+1].reshape((1, size))[0]


def weight(patch1, patch2, h):
    """ Compute weight as per step 2a of Chaudhury Singer (2012) algorithm 1
        e^((-||P1 - P2||^2)/h^2)
        where ||P1 - P2||^2 is the square euclidean distance between patches P1 and P2
        and h is a smoothing factor
    """
    return math.exp(-sqeuclidean(patch1, patch2) / (h * h))


def neighbours(yx, s):
    centre_y, centre_x = yx
    for y in range(centre_y-s, centre_y+s+1, 1):
        for x in range(centre_x-s, centre_x+s, 1):
            yield y, x


def computeNeighbourPatchesAndWeights(patches, pos, h, p, s):
    pos_patch = patches[pos]
    neighbour_patches = np.array([patches[n] for n in neighbours(pos, s)])
    weights = np.array([weight(pos_patch, n, h) for n in neighbour_patches])
    return neighbour_patches, weights / weights.sum()


def singlePixelNLEM(patches, pos, h, p, s):
    weights = np.array([weight(patches[pos], patches[n], h)
                        for n in neighbours(pos, s)])
    ns, ws = computeNeighbourPatchesAndWeights(patches, pos, h, p, s)
    wgmedian = weightedGeometricMedian(ns, weights=ws)
    return wgmedian[len(wgmedian)//2]


def nonLocalEuclideanMediansDenoising(img, h, p, s):
    img = img_as_float(img)
    padding = p + s
    padded = cv.copyMakeBorder(img, padding, padding, padding, padding, cv.BORDER_REFLECT)

    # Compute all the patches
    print("Computing patches... ", end='')
    sys.stdout.flush()
    print("done")
    patches = dict(((y, x), patch(padded, (y, x), p))
                   for y in range(p, img.shape[0] + 2*padding-p, 1)
                   for x in range(p, img.shape[1] + 2*padding-p, 1))

    nlem_img = np.zeros(img.shape)
    for y in range(img.shape[0]):
        print("Row {}/{}: {:4.1f}%".format(y, img.shape[0], y / img.shape[0] * 100))
        py = y + padding
        for x in range(img.shape[1]):
            px = x + padding

            pos = (py, px)
            nlem_img[y, x] = singlePixelNLEM(patches, pos, h, p, s)

    return img_as_ubyte(nlem_img)

def processImage(img, include_original=False):
    """Yield (key, image) pairs for images based on provided image
       Include original, with a key of None, if include_original flag is set
    """
    if include_original:
        yield None, img

    for h in (2,):
        for p in (1,):
            for s in (2,):
                print("Non-local means denoising (with h={}, p={}, s={})...".format(h, p , s))
                nlm_img = cv.fastNlMeansDenoising(img, h, 2*p+1, 2*s+1)
                yield "NLMh{}p{}s{}".format(h, p, s), nlm_img
                yield "NLMh{}p{}s{}Noise".format(h, p, s), img - nlm_img

                print("Non-local *medians* denoising (with h={}, p={}, s={})...".format(h, p , s))
                nlem_img = nonLocalEuclideanMediansDenoising(img, h, p, s)
                yield "NLEMh{}p{}s{}".format(h, p, s), nlem_img
                yield "NLEMh{}p{}s{}Noise".format(h, p, s), img - nlem_img


DEFAULT_FILENAMES = []

def main(args):
    print("NumPy v" + np.__version__)
    print("OpenCV v" + cv.__version__)
    print("scikit-image v" + skimage_version)

    image_filenames = args[1:] or DEFAULT_FILENAMES
    for fn in image_filenames:
        assert os.path.isfile(fn)
    assert image_filenames, "Need at least one input image filename"

    images = dict((fn, cv.imread(fn)) for fn in image_filenames)

    for fn, img in images.items():
        print(fn, img.shape)
        basename, extension = fn.rsplit('.', 1)

        # Verify that this is actually a grayscale image
        b, g, r = cv.split(img)
        assert np.array_equal(b, g), "blue green"
        assert np.array_equal(b, r), "blue red"
        # Convert to gray by using the blue channel (asserted to be identical to red and green)
        img = b

        for key, processed_img in processImage(img):
            out_fn = "{}-{}.{}".format(basename, key or "original", extension)
            print("Writing {!r}".format(out_fn))
            cv.imwrite(out_fn, img_as_ubyte(processed_img))

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
