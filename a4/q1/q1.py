#!/usr/bin/env python3

import sys
import os

import numpy as np
# from skimage import io, data, filters, restoration, img_as_float
# from skimage import __version__ as skimage_version
import cv2 as cv


def processImage(img, include_original=False):
    """Yield (key, image) pairs for images based on provided image
       Include original, with a key of None, if include_original flag is set
    """
    if include_original:
        yield None, img

    print("Gaussian blurring...")
    gauss3_img = cv.GaussianBlur(img, (21, 21), 3)
    yield "Gauss3", gauss3_img
    yield "Gauss3Noise", img - gauss3_img

    for h in range(5, 25, 5): # smoothing parameter
        for p in (3, 7): # patch size
            for s in (10, 27, 100): # search window size (in neighbourhood of patch)
                print("Non-local means denoising (with h={}, p={}, s={})...".format(h, p , s))
                nlm_img = cv.fastNlMeansDenoising(img, h, 2*p+1, 2*s+1)
                yield "NLMh{}p{}s{}".format(h, p, s), nlm_img
                yield "NLMh{}p{}s{}Noise".format(h, p, s), img - nlm_img



DEFAULT_FILENAMES = ["ISO409600_SonyA7SII_lowlight_image.jpg"]

def main(args):
    print("NumPy v" + np.__version__)
    print("OpenCV v" + cv.__version__)
#     print("scikit-image v" + skimage_version)

    image_filenames = args[1:] or DEFAULT_FILENAMES
    for fn in image_filenames:
        assert os.path.isfile(fn)

    images = dict((fn, cv.imread(fn)) for fn in image_filenames)

    for fn, img in images.items():
        print(fn, img.shape)
        basename, extension = fn.rsplit('.', 1)

        # Verify that this is actually a grayscale image
        b, g, r = cv.split(img)
        assert np.array_equal(b, g), "blue green"
        assert np.array_equal(b, r), "blue red"
        # Convert to gray by using the blue channel (asserted to be identical to red and green)
        img = b

        for key, processed_img in processImage(img, include_original=True):
            out_fn = "{}-{}.{}".format(basename, key or "original", extension)
            print("Writing {!r}".format(out_fn))
            cv.imwrite(out_fn, processed_img)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
