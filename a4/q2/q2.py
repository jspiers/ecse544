#!/usr/bin/env python3

import sys
import os

import numpy as np
import cv2 as cv


def processImage(img, include_original=False):
    """Yield (key, image) pairs for images based on provided image
       Include original, with a key of None, if include_original flag is set
    """
    if include_original:
        yield None, img

    for h in (8, 10, 12):
        print("Non-local means denoising (with h={})...".format(h))
        nlm_img = cv.fastNlMeansDenoising(img, h=h)
        yield "NLMh{}".format(h), nlm_img
        yield "NLMh{}Noise".format(h), img - nlm_img


DEFAULT_FILENAMES = ["3_High_ISO_Test_5D_Mark_II_Birds_and_Plaza_Hotel_blurred_noisy.jpg"]

def main(args):
    print("NumPy v" + np.__version__)
    print("OpenCV v" + cv.__version__)
#     print("scikit-image v" + skimage_version)

    image_filenames = args[1:] or DEFAULT_FILENAMES
    for fn in image_filenames:
        assert os.path.isfile(fn)

    images = dict((fn, cv.imread(fn)) for fn in image_filenames)

    for fn, img in images.items():
        print(fn, img.shape)
        basename, extension = fn.rsplit('.', 1)

        # Verify that this is actually a grayscale image
        b, g, r = cv.split(img)
        assert np.array_equal(b, g), "blue green"
        assert np.array_equal(b, r), "blue red"
        # Convert to gray by using the blue channel (asserted to be identical to red and green)
        img = b

        for key, processed_img in processImage(img):
            out_fn = "{}-{}.{}".format(basename, key or "original", extension)
            print("Writing {!r}".format(out_fn))
            cv.imwrite(out_fn, processed_img)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
