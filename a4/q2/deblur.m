
function deblur(input_filename)

    % Extract file basename and extension (assumes 3 character extension)
    basename = input_filename([1:end-4])
    extension = input_filename([(end-2):end])

    img = imread(input_filename);

    % Attempt deconvblind with various Gaussian estimated blur kernels (PSFs)
    for sigma = 2:2
        sigma % print current sigma to show progress

        % Guess a point-spread function (PSF) as a Gaussian
        dim = sigma * 6 + 1; % guaranteed to be odd
        psf_estimate = fspecial('gaussian', dim, sigma);

        % Blind deconvolution using estimated PSF to guide the algorithm
        [deblurred psf]  = deconvblind(img, psf_estimate);

        % Write out deblurred image
        unique = sprintf('deconvblind-gauss%d', sigma);
        deblurred_filename = sprintf('%s-%s.%s', basename, unique, extension)
        imwrite(deblurred, deblurred_filename);

        % Also write out the PSF that deconvblind determined
        %psf_filename = sprintf('%s-%s-psf.%s', basename, unique, extension)
        %imwrite(psf, psf_filename);

        % Populate plot of PSFs
        subplot(220+sigma);
        imshow(psf, [], 'InitialMagnification', 'fit');
        title(sprintf('PSF%d', sigma));

    end
end


