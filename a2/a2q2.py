#!/usr/local/bin/python3

import sys
import os
import numpy as np
import skimage

def processImage(fn):
    print("Processing {!r}".format(fn))
    assert os.path.isfile(fn)

    # Read file and convert to floating point
    #img = np.float64(skimage.io.imread(fn)) / 255
    img = skimage.img_as_float(skimage.io.imread(fn))

    # Extract the eight 20x20 patches
    patches = [img[y:y+19, x:x+19] for (x, y) in PATCH_COORDINATES]

    # Compute average RGB CIE values for each of the patches
    patches_rgbcie = np.array([[[p[:,:,i].mean() for i in range(3)] for p in patches]])
    print("patches_rgbcie\n{}".format(patches_rgbcie))

    # Convert to sRGB
    patches_rgb = skimage.color.rgbcie2rgb(patches_rgbcie)
    print("patches_rgb\n{}".format(patches_rgb))

    # Convert to XYZ using skimage library
    patches_xyz = skimage.color.rgb2xyz(patches_rgb)
    print("patches_xyz\n{}".format(patches_xyz))
#     print("patches_xyz 255\n{}".format(patches_xyz * 255))


IMAGE_FILENAMES = (
    "white_balance_example_color_checkers.jpg",
    "white_balance_example_color_checkers_gw.jpg"
)

# Coordinates of top-right corner of 20x20 samples within the 8 patch regions in the image
PATCH_COORDINATES = (
    (220, 160),  # first patch (as shown in "screenshot-showfoto-example.png")
    (220, 193),
    (220, 225),
    (219, 260),
    (219, 293),
    (219, 327),
    (218, 361),
    (220, 394)
)

def main(args):
#     print("OpenCV v" + cv2.__version__)
    print("scikit-image v" + skimage.__version__)
    for fn in IMAGE_FILENAMES:
        processImage(fn)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
