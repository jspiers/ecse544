#!/usr/local/bin/python3

import sys
import os
import numpy as np
import cv2
import skimage
from functools import partial

# Use functools.partial to create a uint8-specific rescale function
rescale_uint8 = partial(skimage.exposure.rescale_intensity, out_range="uint8")


# Perform white-balancing on img using the Max-RGB algorithm
def wbMaxRGB(img):
    # Divide each channel's values by its max
    return cv2.merge(tuple(channel / channel.max() for channel in cv2.split(img)))


# Perform white-balancing on img using the Grey-World algorithm
def wbGreyWorld(img):
    # Divide each channel's value by its average
    return cv2.merge(tuple(channel / channel.mean() for channel in cv2.split(img)))


# Perform white-balancing on img using the Grey-Edge algorithm
def wbGreyEdge(img, derivative_order=1, minkowski_order=1):
    assert derivative_order in (1, 2)
    assert minkowski_order == 1 # higher order Minkowski norms not yet implemented

    # Split colour channels
    bgr = cv2.split(img)

    # Compute derivatives
    if derivative_order == 1:
        # Use 2D Sobel filter to compute first-order gradient
        derivatives = tuple(cv2.Sobel(channel, cv2.CV_64F, 1, 1, ksize=3) for channel in bgr)
    else: # derivative_order = 2
        # Use Laplacian filter to compute second-order gradient
        derivatives = tuple(cv2.Laplacian(channel, cv2.CV_64F) for channel in bgr)

    # Balance each channel by dividing by the average of the absolute gradient for that channel
    # (i.e. first-order Minkowski norm)
    balanced = tuple(channel / abs(d).mean() for (channel, d) in zip(bgr, derivatives))

    # Merge colours and return
    return cv2.merge(balanced)


def processImageFile(fn):
    print("Processing {!r}".format(fn))
    assert os.path.isfile(fn)

    # Read file and convert to floating point
    img = np.float64(cv2.imread(fn))

    # White balance using Max-RGB
    print("  Max-RGB")
    img_wb_maxrgb = wbMaxRGB(img)

    # Write to file after rescaling back to 8-bit unsigned integers (uint8)
    cv2.imwrite(fn[:-4] + "_maxRGB.jpg", rescale_uint8(img_wb_maxrgb))

    # Now do the same but using Grey World
    print("  Grey-World")
    img_wb_gw = wbGreyWorld(img)
    cv2.imwrite(fn[:-4] + "_gw.jpg", rescale_uint8(img_wb_gw))

    # Now Grey Edge
    for d, m in ((1, 1), (2, 1)):
        print("  Grey-Edge {}-order derivative {}-order Minkowski".format(d, m))
        img_wb_ge = wbGreyEdge(img, d, m)
        cv2.imwrite(fn[:-4] + "_ge_d{}_m{}.jpg".format(d, m), rescale_uint8(img_wb_ge))


DEFAULT_IMAGE_FILENAMES = [
    'White-Balance-Incorrect-850x567.jpg',
    'white_balance_example_color_checkers.jpg',
    '14_poor-white-balance.jpg'
]

def main(args):
    print("OpenCV v" + cv2.__version__)
    print("scikit-image v" + skimage.__version__)

    image_filenames = args[1:] if len(args) > 1 else DEFAULT_IMAGE_FILENAMES

    for fn in image_filenames:
        processImageFile(fn)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
