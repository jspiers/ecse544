#!/usr/local/bin/python3

import sys
import os
import numpy as np
import skimage

def processImage(fn):
    print("Processing {!r}".format(fn))
    assert os.path.isfile(fn)

    # Read file and convert to floating point
#     img = np.float64(skimage.io.imread(fn)) / 255
    img = skimage.io.imread(fn)

    for (x, y) in PATCH_COORDINATES[:1]:
        p = img[y:y+19, x:x+19]
        rgb = [p[:,:,i].mean() for i in range(3)]
        print("rgb {}".format(rgb))
        rgb_max = max(rgb)
        print("rgb_max {}".format(rgb_max))
        rgb_normalized = [channel / max(rgb) for channel in rgb]
        print("rgb_normalized {}".format(rgb_normalized))
        patch = np.array([[[rgb_normalized]]])
        print("patch {}".format(patch))
        xyz = skimage.color.rgb2xyz(patch)
        print("xyz {}".format(xyz))

    return 0

    # Extract the eight 20x20 patches
    patches = [img[y:y+19, x:x+19] for (x, y) in PATCH_COORDINATES]

#     patches_rgb = np.array([[[p[:,:,i].mean() for i in range(3)] for p in patches]])
    patches_rgb = list([p[:,:,i].mean() for i in range(3)] for p in patches)
    print("patches_rgb\n{}".format(patches_rgb))
    patches_rgb_normalized = [[val / max(vals) for val in vals] for vals in patches_rgb]
    print("patches_rgb_normalized\n{}".format(patches_rgb_normalized))
#     return 0

    # Compute average RGB values for each of the patches
    patches_rgb = np.array([[[p[:,:,i].mean() for i in range(3)] for p in patches]])
#     print("patches_rgb\n{}".format(patches_rgb))

    for rgb in patches_rgb_normalized:
        print(rgb)
        patch = np.array([[[rgb]]])
        print("patch {}".format(patch))
        xyz = skimage.color.rgb2xyz(patch)
        print("xyz {}".format(xyz))
    # Convert to XYZ using skimage library
#     patches_xyz = skimage.color.rgb2xyz(patches_rgb)
#     print("patches_xyz\n{}".format(patches_xyz))


IMAGE_FILENAMES = (
    "white_balance_example_color_checkers.jpg",
    "white_balance_example_color_checkers_gw.jpg"
)

# Coordinates of top-right corner of 20x20 samples within the 8 patch regions in the image
PATCH_COORDINATES = (
    (220, 160),  # first patch (as shown in "screenshot-showfoto-example.png")
    (220, 193),
    (220, 225),
    (219, 260),
    (219, 293),
    (219, 327),
    (218, 361),
    (220, 394)
)

def main(args):
#     print("OpenCV v" + cv2.__version__)
    print("scikit-image v" + skimage.__version__)
    for fn in IMAGE_FILENAMES:
        processImage(fn)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
