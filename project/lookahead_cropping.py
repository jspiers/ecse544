#!/usr/bin/env python

import sys
import os
import cv2
from utils import viderator, COLORS, TRACKERS, make_directory

(major_ver, minor_ver, subminor_ver) = [int(v) for v in cv2.__version__.split('.')]
assert major_ver > 2 and minor_ver > 2, \
       "Need OpenCV version >= 3.3, not {!r}".format(cv2.__version__)

def fit_bounding_box(bbox, frame_shape):
    x, y, w, h = bbox
    fh, fw = frame_shape[:2]
    x = min(max(0, x), fw - w)
    y = min(max(0, y), fh - h)
    return x, y, w, h


def normalize_bounding_box(bbox):
    x, y, w, h = [int(round(val)) for val in bbox]
    assert w and h, bbox
    if w < 0:
        x += w
        w = -w
    if h < 0:
        y += h
        h = -h
    return x, y, w, h


def expand_bounding_box(bbox, shape):
    x, y, w, h = bbox
    fw, fh = shape
    w_diff = fw - w
    h_diff = fh - h
    return (x - w_diff//2, y - h_diff//2, fw, fh)


def adjust_bounding_box(bbox, shape):
#     print("original", bbox)
#     print("shape", shape)
    normalized = normalize_bounding_box(bbox)
#     print("normalized", normalized)
    expanded = expand_bounding_box(normalized, shape)
#     print("expanded", expanded)
    return expanded


def crop(img, bbox):
    x, y, w, h = bbox
    return img[y:y+h, x:x+w]


def draw_box(img, bbox, color, note=None):
    x, y, w, h = normalize_bounding_box(bbox)
    p1 = (x, y)
    p2 = (x + w, y + h)
    augmented = img.copy()
    cv2.rectangle(augmented, p1, p2, color, 2, 1)
    if note:
        cv2.putText(augmented, note, (x+5, y+20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    return augmented


def yield_tracking_box(frames, tracker_type, monitor):
    # Initialize tracker
    tracker = TRACKERS[tracker_type]()

    track_box = None
    for frame in frames:
        # First frame
        if track_box is None:
            # Ask user to select bounding box around object to be tracked
            track_box = cv2.selectROI(frame, False)

            # Initialize tracker with first frame and bounding box
            tracker.init(frame, track_box)

        # Subsequent frames
        else:
            # Update tracking box based on new frame
            _, track_box = tracker.update(frame)

        # Optionally display tracking as we go
        if monitor:
            cv2.imshow("Tracking", draw_box(frame, track_box, COLORS["blue"], tracker_type))

            # Exit if ESC or 'q' is pressed
            k = cv2.waitKey(1) & 0xff
            if k in (27, ord('q')):
                break

        yield track_box


def compute_crop_shape_log2(roi_shape, frame_shape):
    """ Computes the cropping shape as largest power of 2 division
        of frame_shape large enough to fit roi_shape
    """
    fw, fh = frame_shape
    w, h = roi_shape
    while fh >= 2*h and fw >= 2*w:
        fh //= 2
        fw //= 2
    return (fw, fh)


def track(video_filename, tracker_type, scale_factor, write, monitor):
    # Read video and its properties
    vidcap = cv2.VideoCapture(video_filename)
    frame_w = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_h = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = vidcap.get(cv2.CAP_PROP_FPS)
    fourcc = int(vidcap.get(cv2.CAP_PROP_FOURCC))

    # Create frame iterator
    frames = viderator(vidcap)

    # Downscale
    if scale_factor is not None:
        assert 0.001 < scale_factor < 1.0, "Scaling must be < 1 (got {})".format(scale_factor)
        frames = iter(cv2.resize(f, None, fx=scale_factor, fy=scale_factor) for f in frames)

    # Read all the frames into a list
    print("Reading video frames...")
    frames = list(frames)

    # Compute tracking boxes for all frames
    print("Tracking selected object...")
    track_boxes = list(yield_tracking_box(frames, tracker_type, monitor))

    # Find maximum tracking box dimensions
    max_w = max(w for (_, _, w, _) in track_boxes)
    max_h = max(h for (_, _, _, h) in track_boxes)

    # Compute the size of the cropping window based on max tracking box and frame shape
    crop_w, crop_h = compute_crop_shape_log2((max_w, max_h), (frame_w, frame_h))

    # Initialize the augmented and cropped video output
    if write:
        basename, extension = video_filename.rsplit('.', 1)
        dirname = make_directory(basename + "_lookahead")
        augmented_filename = os.path.join(dirname, "{}_augmented.{}".format(basename, extension))
        print("Writing {}x{} augmented video to {!r}".format(frame_w, frame_h, augmented_filename))
        augmented_h, augmented_w = frames[0].shape[:2]
        augmented_video = cv2.VideoWriter(augmented_filename, fourcc, fps, (augmented_w, augmented_h))
        cropped_filename = os.path.join(dirname, "{}_cropped.{}".format(basename, extension))
        print("Writing {}x{} cropped video to {!r}".format(crop_w, crop_h, cropped_filename))
        cropped_video = cv2.VideoWriter(cropped_filename, fourcc, fps, (crop_w, crop_h))

    for frame, track_box in zip(frames, track_boxes):
        # Augment frame by drawing the tracking bounding box
        augmented = draw_box(frame, track_box, COLORS["blue"], tracker_type)

        # Adjust cropping based on the tracking box
        crop_box = fit_bounding_box(adjust_bounding_box(track_box, (crop_w, crop_h)), frame.shape)
        cropped = crop(frame, crop_box)

        # Draw the cropping box on the augmented frame
        augmented = draw_box(augmented, crop_box, COLORS["white"])

        # Write to video files
        if write:
            augmented_video.write(augmented)
            cropped_video.write(cropped)

        # Display results
        else:
            cv2.imshow("Augmented", augmented)
            cv2.imshow("Cropped", cropped)

            # Exit if ESC or 'q' is pressed
            k = cv2.waitKey(1) & 0xff
            if k in (27, ord('q')):
                break

    if write:
        augmented_video.release()
        cropped_video.release()

    return 0


if __name__ == '__main__':

    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("video_filename")
    ap.add_argument("-s", "--scale_factor", type=float, default=None, help="downscaling factor")
    ap.add_argument("-t", "--tracker_type", type=str, default="CSRT", choices=TRACKERS.keys(),
                    help="tracker type")
    ap.add_argument("-w", "--write", action="store_true",
                    help="write augmented and cropped videos to file")
    ap.add_argument("-m", "--monitor", action="store_true",
                    help="monitor tracking augmented video during tracking")
    sys.exit(track(**vars(ap.parse_args())))
