#!/usr/bin/env python

import sys
import cv2
from utils import viderator, COLORS, INDEX_COLORS, TRACKERS

(major_ver, minor_ver, subminor_ver) = [int(v) for v in cv2.__version__.split('.')]
assert major_ver > 2 and minor_ver > 2, \
       "Need OpenCV version >= 3.3, not {!r}".format(cv2.__version__)


def fit_bounding_box(bbox, frame_shape):
    x, y, w, h = bbox
    fh, fw = frame_shape[:2]
    x = min(max(0, x), fw - w)
    y = min(max(0, y), fh - h)
    return x, y, w, h


def normalize_bounding_box(bbox):
    x, y, w, h = [int(round(val)) for val in bbox]
    assert w and h, bbox
    if w < 0:
        x += w
        w = -w
    if h < 0:
        y += h
        h = -h
    return x, y, w, h


def expand_bounding_box(bbox, shape):
    x, y, w, h = bbox
    fh, fw = shape[:2]
    while fh >= 2*h and fw >= 2*w:
        fh //= 2
        fw //= 2
    w_diff = fw - w
    h_diff = fh - h
    return (x - w_diff//2, y - h_diff//2, fw, fh)


def adjust_bounding_box(bbox, shape):
    normalized = normalize_bounding_box(bbox)
    expanded = expand_bounding_box(normalized, shape)
    fitted = fit_bounding_box(expanded, shape)
    return fitted


def crop(img, bbox):
    x, y, w, h = bbox
    return img[y:y+h, x:x+w]


def draw_box(img, bbox, color):
    x, y, w, h = normalize_bounding_box(bbox)
    p1 = (x, y)
    p2 = (x + w, y + h)
    cv2.rectangle(img, p1, p2, color, 2, 1)


def preprocess(img):
    " Placeholder for preprocessing to perform before tracking "
    return img


def track(video_filename, tracker_types, downscale_factor):
    # Set up trackers
    trackers = dict((k, TRACKERS[k]()) for k in tracker_types)
    tracker_colors = dict(zip(trackers.keys(), [COLORS[k] for k in INDEX_COLORS]))

    # Read video
    video = cv2.VideoCapture(video_filename)

    # Exit if video not opened.
    if not video.isOpened():
        return "Could not open video"

    frames = viderator(video)
    if downscale_factor is not None:
        assert 0.001 < downscale_factor < 1.0
        frames = iter(cv2.resize(f, None, fx=downscale_factor, fy=downscale_factor)
                      for f in frames)

    track_boxes = {}
    track_status = {}
    for frame in frames:
        if not track_boxes:
            # Ask user to select bounding box around object to be tracked
            track_box = cv2.selectROI(frame, False)
            track_boxes = dict((k, track_box) for k in trackers.keys())

            # Initialize tracker with first frame and bounding box
            for k, tracker in trackers.items():
                if not tracker.init(preprocess(frame), track_box):
                    return k + " failed to initialize"
            track_status = dict((k, True) for k in trackers.keys())
        else:
            # Update tracker
            for k, tracker in trackers.items():
                track_status[k], track_boxes[k] = tracker.update(preprocess(frame))

        # Augment frame by drawing the tracking bounding box
        failed = []
        text_x = 10
        text_y = 20
        for k, ok in track_status.items():
            if ok:
                # Tracking success, draw the tracking box
                track_box = track_boxes[k]
                color = tracker_colors[k]
                draw_box(frame, track_box, color)
                cv2.putText(frame, k, (text_x, text_y),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
                text_y += 20
            else:
                failed.append(k)

        if failed:
            # Tracking failure
            cv2.putText(frame, ' '.join(failed) + " failed", (text_x, text_y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS["red"], 2)
            text_y += 20

        # Display results
        cv2.imshow("Tracker Comparison", frame)

        # Exit if ESC or 'q' is pressed
        k = cv2.waitKey(1) & 0xff
        if k in (27, ord('q')):
            break

    return 0


if __name__ == '__main__':

    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("video_filename")
    ap.add_argument("-d", "--downscale_factor", type=float, default=None, help="downscaling factor")
    ap.add_argument("-t", "--tracker_types", nargs='*', default=TRACKERS.keys(), choices=TRACKERS.keys(), help="tracker types")
    sys.exit(track(**vars(ap.parse_args())))
