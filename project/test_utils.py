import unittest
import utils

class TestUtils(unittest.TestCase):
    def test_digits(self):
        self.assertEqual(utils.digits(0), 1)
        self.assertEqual(utils.digits(1), 1)
        self.assertEqual(utils.digits(9), 1)
        self.assertEqual(utils.digits(10), 2)
        self.assertEqual(utils.digits(99) ,2)
        self.assertEqual(utils.digits(100), 3)

    @unittest.expectedFailure
    def test_fail(self):
        self.fail()

if __name__ == "__main__":
    unittest.main()
