#!/usr/bin/env python3

import sys
import os
import argparse
import json
from itertools import count

import cv2

from utils import make_directory, digits


def yield_sorted_images(directory, extensions=("jpg", "png")):
    " Sort the images in the directory by filename and yield them as images one by one "
    assert os.path.isdir(directory)
    for fn in sorted([fn for fn in os.listdir(directory) if fn.endswith(extensions)]):
        yield cv2.imread(os.path.join(directory, fn))


def default_video_filename(hint="video", extension="mp4"):
    " Find an available filename similar (or identical) to 'hint' "
    for suffix in (('_' + str(i)) if i else '' for i in count()):
        video_filename = "{}{}.{}".format(hint, suffix, extension)
        if not os.path.exists(video_filename):
            return video_filename


def extract_frames(video_filename, output_dir, extension="jpg"):
    " Extract the frames from 'video_filename' and store them in 'output_dir' "
    assert os.path.isfile(video_filename)
    assert os.path.isdir(output_dir), "directory {!r} does not exist".format(output_dir)

    # Open video file and extract info about the file, save it in json
    vidcap = cv2.VideoCapture(video_filename)
    frame_count = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    info = {
        #"frame_count": frame_count,
        #"width": int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
        #"height": int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),
        "fps": vidcap.get(cv2.CAP_PROP_FPS),
        "fourcc": int(vidcap.get(cv2.CAP_PROP_FOURCC))
    }
    with open(os.path.join(output_dir, 'info.json'), 'w') as f:
        json.dump(info, f)

    # Print a status update
    headline = "Extracting {} frames of {!r} to directory {!r}"
    print(headline.format(frame_count, video_filename, output_dir))

    # Read frames from the video and write them to image files
    filename_template = "frame{{:0{}d}}.{}".format(digits(frame_count), extension)
    for i in count(1):
        success, image = vidcap.read()
        if not success:
            break
        fn = os.path.join(output_dir, filename_template.format(i))
        cv2.imwrite(fn, image)

        # Print current frame count every 50 frames, else just a '.'
        if i % 50:
            print('.', end='')
            sys.stdout.flush()
        else:
            print(i)

    return 0


def assemble_frames(directory, video_filename, default_fps=25.0):
    assert os.path.isdir(directory), "{!r} is not a directory".format(directory)

    # Get framerate and 4cc code from info.json else use defaults
    json_filename = os.path.join(directory, "info.json")
    if os.path.isfile(json_filename):
        with open(json_filename) as f:
            info = json.load(f)
        fps = info["fps"]
        fourcc = info["fourcc"]
    else:
        # use defaults
        info = {}
        fps = default_fps
        assert video_filename.lower().endswith(".mp4"), \
               "Target filename must be a .mp4 file (not {!r})".format(video_filename)
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")

    # Get an iterator of the images from source directory in sorted (by filename) order
    image_iter = iter(yield_sorted_images(directory))

    # Make sure they are all the same geometry and number of channels
    image = next(image_iter)
    height, width, channels = image.shape

    headline = "Assembling frames from {!r} into video {!r} at {}fps"
    print(headline.format(directory, video_filename, fps))

    # Assemble the video
    video = cv2.VideoWriter(video_filename, fourcc, fps, (width, height))
    for i, image in enumerate(image_iter, 1):
        assert image.shape == (height, width, channels), \
               "Frame {} has mismatched shape {} (should be {} like the others)".format(\
               i, image.shape, (height, width, channels))
        video.write(image)
        if i % 50:
            print('.', end='')
            sys.stdout.flush()
        else:
            print(i)
    video.release()
    return 0


def main(source, target, extension):
    if os.path.isdir(source):
        filename = target or default_video_filename(source.rstrip('/'))
        return assemble_frames(source.rstrip('/'), filename)
    elif os.path.isfile(source):
        directory = target.rstrip('/') if target else make_directory(source.rsplit('.', 1)[0])
        return extract_frames(source, directory, extension)
    else:
        return "No such file or directory {!r}".format(source)


if __name__ == "__main__":
    desc = "Extract frames from or assemble frames into a video"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("source", metavar="source")
    parser.add_argument("-o", dest="target", nargs='?', default=None)
#     parser.add_argument("-v", dest="verbose", action="store_true")
    parser.add_argument("-x", "--extension", default="jpg", help="Image filename extension")
    sys.exit(main(**vars(parser.parse_args())))
