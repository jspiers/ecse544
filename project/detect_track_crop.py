#!/usr/bin/env python3

# Adapted from www.pyimagesearch.com

import sys
import logging
from collections import defaultdict
from math import sqrt
import cv2
from utils import viderator, COLORS, INDEX_COLORS, TRACKERS, resize

logging.basicConfig(format="%(filename)-10s line%(lineno)4s: %(levelname)-7s: %(message)s",
                    level=logging.INFO)


def fit_bounding_box(box, shape):
    """ Moves bounding box to make sure it is contained within
        a frame of the given shape
    """
    x, y, w, h = box
    fh, fw = shape[:2]
    x = min(max(0, x), fw - w)
    y = min(max(0, y), fh - h)
    return x, y, w, h


def normalize_bounding_box(box):
    """ Returns an equivalent bounding box with integer coordinates
        and positive height, width
    """
    x, y, w, h = [int(round(val)) for val in box]
    assert w and h, box
    if w < 0:
        x += w
        w = -w
    if h < 0:
        y += h
        h = -h
    return x, y, w, h


def expand_bounding_box(box, shape):
    """ Returns a new bounding box centered on the given box but of the given shape
        Bounding box must have integer coordinates and positive height, width
    """
    x, y, w, h = box
    fw, fh = shape
    w_diff = fw - w
    h_diff = fh - h
    return (x - w_diff//2, y - h_diff//2, fw, fh)


def adjust_bounding_box(box, shape):
    """ Returns a new bounding box centered on the given box but of the given shape
        Given box may have floating point coordinates and negative height and/or width
    """
    normalized = normalize_bounding_box(box)
    expanded = expand_bounding_box(normalized, shape)
    return expanded


def compute_crop_shape_log2(roi_shape, frame_shape):
    """ Computes the cropping shape as largest power of 2 division
        of frame_shape large enough to fit roi_shape
    """
    fw, fh = frame_shape
    w, h = roi_shape
    while fh >= 2*h and fw >= 2*w:
        fh //= 2
        fw //= 2
    return (fw, fh)


def crop(img, box):
    " Extracts the box from the image as a new image "
    x, y, w, h = box
    return img[y:y+h, x:x+w]


def center(box):
    " Returns the point nearest the center of a box "
    x, y, w, h = box
    return (x + w//2, y + h//2)


def distance(p1, p2):
    " Returns the distance between two points "
    x1, y1 = p1
    x2, y2 = p2
    dx = x1 - x2
    dy = y1 - y2
    return sqrt(dx*dx + dy*dy)


def draw_box(img, box, color, note=None):
    """ Returns new image with a box of the given color drawn on it,
        along with an optional note in the box's top left corner
    """
    x, y, w, h = normalize_bounding_box(box)
    p1 = (x, y)
    p2 = (x + w, y + h)
    augmented = img.copy()
    cv2.rectangle(augmented, p1, p2, color, 2, 1)
    if note:
        cv2.putText(augmented, note, (x+5, y+20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    return augmented


def edge_distance(box, img):
    " Find the smallest distance from the box to the edge of the image "
    x, y, w, h = box
    return min(x, y, img.shape[0] - (y+h), img.shape[1] - (x+w))


class MotionDetector:

    @staticmethod
    def deltaThreshold(delta, minlevel=10):
        " Apply binary thresholding to a frame-to-frame delta "
        thresh = cv2.threshold(delta, minlevel, 255, cv2.THRESH_BINARY)[1]
        # Dilate to increase density
        dilated = cv2.dilate(thresh, None, iterations=4)
        return dilated

    def __init__(self, object_ppm, monitor=None):
        self._prev = None
        self._object_ppm = object_ppm
        self._monitor = monitor

    def preprocess(self, frame):
        """ Preprocess the frame to make it suitable for comparison with other
            frames in motion detection and object tracking
        """
        # convert to grayscale and blur
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        # show the preprocessed (blurred) frame
        self._monitor.put("Preprocessed", gray)

        return gray

    def update(self, frame):
        frame = self.preprocess(frame)

        if self._prev is not None:

            # Compute the absolute difference from the previous frame
            delta = cv2.absdiff(self._prev, frame)
            self._monitor.put("Delta", delta)

            # Apply thresholding
            thresh = self.deltaThreshold(delta)
            self._monitor.put("Threshold", thresh)

            # Find contours
            contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours = contours[1]

            # Filter out:
            # (1) small objects (might be noise)
            # (2) objects too close to the edge of the frame (might not be fully in yet)
            for c in contours:
                if cv2.contourArea(c) / thresh.size >= self._object_ppm / 1e6:
                    box = cv2.boundingRect(c)
                    if edge_distance(box, frame) > 10:
                        yield box

        self._prev = frame


class ObjectTracker:
    MOTION_DISTANCE_THRESHOLD = 120
    STAGNATION_FRAME_COUNT = 4
    STAGNATION_TRAVEL_THRESHOLD = 3
    STAGNATION_EDGE_DISTANCE = 2

    def __init__(self, monitor=None):
        self._frames = []
        self._next_id = 0
        self._trackers = {}
        self._objects = defaultdict(list)
        self._monitor = monitor
        self._travel = defaultdict(list)
        self._start_frame = {}
        self._retired = {}

    def updateMotion(self, motion_box):
        " Link motion either to a current object or a new one "
        c = center(motion_box)
        for id_, obj_boxes in self._objects.items():
            if distance(c, center(obj_boxes[-1])) < self.MOTION_DISTANCE_THRESHOLD:
                logging.debug("Motion at %r %r linked to object %r",
                              motion_box, center(motion_box), id_)
                break
        else:
            id_ = self._addObject(motion_box)
            logging.info("New object id %r for motion at %r %r",
                         id_, motion_box, center(motion_box))

        return id_

    def _addObject(self, box, tracker_type="CSRT"):
        " Set up tracking of a new object "
        id_ = self._next_id
        self._next_id += 1
        self._objects[id_].append(box)
        self._start_frame[id_] = len(self._frames)
        tracker = TRACKERS[tracker_type]()
        tracker.init(self._frames[-1], box)
        self._trackers[id_] = tracker
        return id_

    def retireObject(self, id_):
        self._retired[id_] = self._objects.pop(id_)
        self._trackers.pop(id_)
        self._travel.pop(id_)

    def retireAllObjects(self):
        for id_ in list(self._objects.keys()):
            self.retireObject(id_)

    def _retireStagnantObjects(self):
        " Detect and retire objects that have probably exited the scene "
        frame = self._frames[-1]
        for id_, boxes in list(self._objects.items()):

            # Retire object it its center is off screen
            x, y = center(boxes[-1])
            fh, fw = frame.shape[:2]
            if not (0 < x < fw and 0 < y < fh):
                logging.info("Object %r %r is off screen", id_, (x, y))
                self.retireObject(id_)
                continue

            # Retire the tracker if it's at the edge of the frame and hasn't moved much lately
            travel = self._travel[id_]
            sfc = self.STAGNATION_FRAME_COUNT
            if len(travel) > sfc and sum(travel[-sfc:]) < self.STAGNATION_TRAVEL_THRESHOLD:
                logging.info("Object %r %r stopped moving", id_, (x, y))
                if edge_distance(fit_bounding_box(boxes[-1], frame.shape[:2]), frame) < self.STAGNATION_EDGE_DISTANCE:
                    logging.info("... near the edge. DELETE")
                    self.retireObject(id_)

    def _showObjects(self):
        " Draw boxes around objects in the most recent frame and display the result "
        objects_frame = self._frames[-1].copy()
        for id_, boxes in self._objects.items():
            color = COLORS[INDEX_COLORS[id_ % len(INDEX_COLORS)]]
            objects_frame = draw_box(objects_frame, boxes[-1], color, str(id_))
        self._monitor.put("Objects", objects_frame)

    def updateFrame(self, frame):
        " Update trackers based on the new frame "

        self._frames.append(frame)
        if self._monitor is not None:
            self._showObjects()

        for id_, tracker in self._trackers.items():
            ok, box = tracker.update(frame)
            if ok:
                prev_box = self._objects[id_][-1]
                self._objects[id_].append(box)

                # Measure the distance traveled by the object
                travel = distance(center(prev_box), center(box))
            else:
                travel = 0
            # Update the list of distances traveled by the object
            self._travel[id_].append(travel)

        # Detect and retire objects that have probably exited the scene
        self._retireStagnantObjects()

    def getCurrentObjects(self):
        " Return an iterable of current active objects "
        return self._objects.items()

    def getRetiredObjects(self):
        " Return an iterable over objects that have exited the scene "
        return self._retired.items()

    def getStartFrame(self, id_):
        " Return the frame number of the first frame for object with given ID "
        return self._start_frame.get(id_, None)


class Monitor:

    def __init__(self, keys=(), scale=1.0):
        self._scale = scale
        self._keys = set(keys)

    def put(self, label, img):
        if label in self._keys:
            cv2.imshow(label, resize(img, scale=self._scale))
        else:
            logging.debug("Monitor ignoring %r", label)

    @staticmethod
    def wait(delay, key=None):
        got = cv2.waitKey(delay)
        return (key is not None) and (got & 0xFF == ord(key))


class Tracker:
    def __init__(self, object_ppm, monitor):
        self._monitor = monitor
        self._ot = ObjectTracker(monitor)
        self._md = MotionDetector(object_ppm, monitor)

    def update(self, frame):
        " Update trackers, then detect any new motion and link it with trackers "
        self._ot.updateFrame(frame)
        for motion_box in self._md.update(frame):
            self._ot.updateMotion(motion_box)

    def track(self, vidcap):
        " Perform motion detection and object tracking for a cv2.VideoCapture object "
        # Loop over the frames in the video
        for frame in viderator(vidcap):

            # Update the motion detector and object tracker with each new frame
            self.update(frame)

            # Break if user hits the 'q' key
            if self._monitor.wait(1, 'q'):
                break

        # Tracking is complete, retire all remaining objects
        self._ot.retireAllObjects()

    def getCurrentObjects(self):
        " Return an iterable of current active objects "
        return self._ot.getCurrentObjects()

    def getRetiredObjects(self):
        " Return an iterable over objects that have exited the scene "
        return self._ot.getRetiredObjects()

    def getStartFrame(self, id_):
        " Return the frame number of the first frame for object with given ID "
        return self._ot.getStartFrame(id_)


def detect_motion(video_filename, object_ppm, scale, max_width, show_keys):

    # Open the video file and get info
    vidcap = cv2.VideoCapture(video_filename)
    frame_w = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_h = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = vidcap.get(cv2.CAP_PROP_FPS)
    fourcc = int(vidcap.get(cv2.CAP_PROP_FOURCC))

    # Compute scaling factor
    scaling_factor = min(scale, (max_width or scale*frame_w)/ frame_w)
    logging.info("Scaling factor %4.2f", scaling_factor)

    # Initialize the monitor
    monitor = Monitor(show_keys, scaling_factor)

    # Initialize motion tracker
    tracker = Tracker(object_ppm, monitor)

    # Track all the objects
    tracker.track(vidcap)

    # Initialize output video of objects in series
    basename, extension = video_filename.rsplit('.', 1)
    fn = "{}_series.{}".format(basename, extension)
    video = cv2.VideoWriter(fn, fourcc, fps, (frame_w, frame_h))

    # Append each object's frames to the series video
    for id_, boxes in tracker.getRetiredObjects():
        start_frame = tracker.getStartFrame(id_)

        # Find maximum tracking box dimensions
        max_w = max(w for (_, _, w, _) in boxes)
        max_h = max(h for (_, _, _, h) in boxes)

        # Compute the size of the cropping window based on max tracking box and frame shape
        crop_w, crop_h = compute_crop_shape_log2((max_w, max_h), (frame_w, frame_h))

        # ... but set a lower bound at 4x
        crop_w = max(crop_w, frame_w // 4)
        crop_h = max(crop_h, frame_h // 4)

        # Report
        logging.info("Cropping (%d, %d) from frame %d for object %r",
                     crop_w, crop_h, start_frame, id_)

        # Rewind
        vidcap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
        for box, frame in zip(boxes, viderator(vidcap)):
            # Fit the crop box within frame
            adjusted_box = adjust_bounding_box(box, (crop_w, crop_h))
            crop_box = fit_bounding_box(adjusted_box, (frame_h, frame_w))

            # Crop
            cropped = crop(frame, crop_box)
            monitor.put("Cropped", cropped)

            # Upscale
            upscaled = resize(cropped, width=frame_w)
            monitor.put("Upscaled", upscaled)

            # Write to file
            video.write(upscaled)

            # Show
            monitor.wait(1)

    # Cleanup
    vidcap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # Construct the argument parser and parse the arguments
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("video_filename", help="path to the video file")
    ap.add_argument("--ppm", dest="object_ppm", type=float, default=2000,
                    help="minimum parts-per-million of detected object size over total frame area")
    ap.add_argument("-d", "--scale", type=float, default=1.0, help="downscaling factor")
    ap.add_argument("--max_width", type=int, default=None, help="maximum frame width")
    ap.add_argument("-s", "--show", dest="show_keys", nargs='+', default=(), help="things to show")
    sys.exit(detect_motion(**vars(ap.parse_args())))
