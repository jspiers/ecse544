#!/usr/bin/env python

import sys
import os
import cv2
from utils import viderator, COLORS, TRACKERS, make_directory, digits

(major_ver, minor_ver, subminor_ver) = [int(v) for v in cv2.__version__.split('.')]
assert major_ver > 2 and minor_ver > 2, \
       "Need OpenCV version >= 3.3, not {!r}".format(cv2.__version__)

def fit_bounding_box(bbox, frame_shape):
    x, y, w, h = bbox
    fh, fw = frame_shape[:2]
    x = min(max(0, x), fw - w)
    y = min(max(0, y), fh - h)
    return x, y, w, h


def normalize_bounding_box(bbox):
    x, y, w, h = [int(round(val)) for val in bbox]
    assert w and h, bbox
    if w < 0:
        x += w
        w = -w
    if h < 0:
        y += h
        h = -h
    return x, y, w, h


def expand_bounding_box(bbox, shape):
    x, y, w, h = bbox
    fh, fw = shape[:2]
    while fh >= 2*h and fw >= 2*w:
        fh //= 2
        fw //= 2
    w_diff = fw - w
    h_diff = fh - h
    return (x - w_diff//2, y - h_diff//2, fw, fh)


def adjust_bounding_box(bbox, shape):
#     print("original", bbox)
#     print("shape", shape)
    normalized = normalize_bounding_box(bbox)
#     print("normalized", normalized)
    expanded = expand_bounding_box(normalized, shape)
#     print("expanded", expanded)
    fitted = fit_bounding_box(expanded, shape)
#     print("fitted", fitted)
    return fitted


def crop(img, bbox):
    x, y, w, h = bbox
    return img[y:y+h, x:x+w]


def draw_box(img, bbox, color):
    x, y, w, h = normalize_bounding_box(bbox)
    p1 = (x, y)
    p2 = (x + w, y + h)
    cv2.rectangle(img, p1, p2, color, 2, 1)


def upscale(img, shape):
    h, w = shape[:2]
    return cv2.resize(img, (w, h), interpolation=cv2.INTER_CUBIC)
#     return cv2.resize(img, (w, h), interpolation=cv2.INTER_LINEAR)



def track(video_filename, tracker_type, scale_factor, upscaling, write):
    # Set up tracker.
    tracker = TRACKERS[tracker_type]()

    # Read video
    vidcap = cv2.VideoCapture(video_filename)
    frame_count = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_w = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_h = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = vidcap.get(cv2.CAP_PROP_FPS)
    fourcc = int(vidcap.get(cv2.CAP_PROP_FOURCC))

    frames = viderator(vidcap)
    if scale_factor is not None:
        assert 0.001 < scale_factor < 1.0
        frames = iter(cv2.resize(f, None, fx=scale_factor, fy=scale_factor) for f in frames)
        augmented_w, augmented_h = [int(round(scale_factor * n)) for n in (frame_w, frame_h)]
    else:
        augmented_w, augmented_h = (frame_w, frame_h)

    # Initialize the augmented and cropped video output
    if write:
        basename, extension = video_filename.rsplit('.', 1)
        out_dir = make_directory(basename + "_dynamic_cropping")

        augmented_fn = os.path.join(out_dir, "{}_augmented.{}".format(basename, extension))
        print("Writing {}x{} augmented video to {!r}".format(frame_w, frame_h, augmented_fn))
        augmented_video = cv2.VideoWriter(augmented_fn, fourcc, fps, (augmented_w, augmented_h))

        cropped_fn_fmt = os.path.join(out_dir, "frame{{:0{}d}}.png".format(digits(frame_count)))

#         cropped_filename = "{}_cropped.{}".format(basename, extension)
#         print("Writing {}x{} cropped video to {!r}".format(crop_w, crop_h, cropped_filename))
#         cropped_video = cv2.VideoWriter(cropped_filename, fourcc, fps, (crop_w, crop_h))


    track_box = None
    for i, frame in enumerate(frames):
        if track_box is None:
            # Ask user to select bounding box around object to be tracked
            track_box = cv2.selectROI(frame, False)

            # Initialize tracker with first frame and bounding box
            if not tracker.init(frame, track_box):
                return "Tracker failed to initialize"

            ok = True
        else:
            # Update tracker
            ok, track_box = tracker.update(frame)

        # Augment frame by drawing the tracking bounding box
        augmented = frame.copy()
        if ok:
            # Tracking success, draw the tracking box
            color = COLORS["blue"]
            draw_box(augmented, track_box, color)
            x, y = normalize_bounding_box(track_box)[:2]
            cv2.putText(augmented, tracker_type, (x+5, y+20),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

            # Adjust cropping based on new tracking box
            crop_box = adjust_bounding_box(track_box, frame.shape)

        else:
            # Tracking failure
            cv2.putText(augmented, tracker_type + " tracking failed", (20, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS["red"], 2)

        # Draw the cropping box
        draw_box(augmented, crop_box, COLORS["white"])

        # Display results
        cv2.imshow("Augmented", augmented)
        cropped = crop(frame, crop_box)
        cv2.imshow("Cropped", cropped)

        if write:
            augmented_video.write(augmented)
            cv2.imwrite(cropped_fn_fmt.format(i), cropped)

        if upscaling:
            upscaled = upscale(cropped, frame.shape)
            cv2.imshow("Upscaled", upscaled)

        # Exit if ESC or 'q' is pressed
        k = cv2.waitKey(1) & 0xff
        if k in (27, ord('q')):
            break

    return 0


if __name__ == '__main__':

    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("video_filename")
    ap.add_argument("-s", "--scale_factor", type=float, default=None,
                    help="downscaling factor")
    ap.add_argument("-t", "--tracker_type", type=str, default="CSRT", choices=TRACKERS.keys(),
                    help="tracker type")
    ap.add_argument("-u", "--upscale", dest="upscaling", action="store_true",
                    help="upscale cropped frames back to original dimensions")
    ap.add_argument("-w", "--write", action="store_true",
                    help="write augmented and cropped videos to file")
    sys.exit(track(**vars(ap.parse_args())))
