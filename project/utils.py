import cv2
import os

COLORS = {
    "black": (0, 0, 0),
    "white": (255, 255, 255),
    "blue": (255, 0, 0),
    "green": (0, 255, 0),
    "red": (0, 0, 255),
    "pink": (255, 0, 255),
    "teal": (255, 255, 0),
    "yellow": (0, 255, 255),
    "orange": (0, 128, 255)
}

INDEX_COLORS = ("blue", "pink", "yellow", "teal", "orange", "green", "red", "white", "black")

TRACKERS = {
    'BOOSTING': cv2.TrackerBoosting_create,
    'MIL': cv2.TrackerMIL_create,
    'KCF': cv2.TrackerKCF_create,
    'TLD': cv2.TrackerTLD_create,
    'MEDIANFLOW': cv2.TrackerMedianFlow_create,
    #'GOTURN': cv2.TrackerGOTURN_create,  # Requires Caffe model: https://github.com/opencv/opencv_extra/tree/c4219d5eb3105ed8e634278fad312a1a8d2c182d/testdata/tracking
    'MOSSE': cv2.TrackerMOSSE_create,
    'CSRT': cv2.TrackerCSRT_create
}

def resize(img, scale=None, width=None, height=None):
    h, w = img.shape[:2]
    num_args = sum(1 if a is not None else 0 for a in (width, height, scale))
    assert num_args == 1, "resize() takes one and only one of 'scale', 'width', 'height' (got {})".format(num_args)
    scale = scale if scale is not None else width/w if width is not None else height/h
    if scale == 1.0:
        return img
    interp = cv2.INTER_LANCZOS4 if scale > 1 else cv2.INTER_AREA
    return cv2.resize(img, None, fx=scale, fy=scale, interpolation=interp)


def viderator(video_capture):
    while True:
        ok, frame = video_capture.read()
        if not ok:
            break
        yield frame

def make_directory(hint="frames"):
    """ Create a directory with a name similar (or identical) to 'hint',
        return the directory name
    """
    dirname = hint
    suffix = 1
    while os.path.exists(dirname):
        dirname = "{}_{:d}".format(hint, suffix)
        suffix += 1
    os.makedirs(dirname)
    return dirname

def digits(n):
    " Return the number of decimal digits needed to represent 'n' "
    return len(str(n))
